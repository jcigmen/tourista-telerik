(function($, doc) {
    var _mapElement;
    var _mapObj;
    var _mapElem
    
    _private = {
        initMap : function(position) {
            _mapElem = document.getElementById("map");
            
			//Delcare function variables
			var myOptions,
    			mapObj = _mapObj,
    			mapElem = _mapElem,
    			pin,
    			locations = [],
                latlng;

			_mapElem = mapElem; //Cache DOM element
                
			// Use Google API to get the location data for the current coordinates
/*			latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
*/				
            latlng = new google.maps.LatLng(14.55, 121.0333);

			myOptions = {
				zoom: 12,
				center: latlng,
				mapTypeControl: false,
				navigationControlOptions: { style: google.maps.NavigationControlStyle.SMALL },
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			    
			mapObj = new google.maps.Map(mapElem, myOptions);
			_mapObj = mapObj; //Cache at app level
        },
        
        getLocation: function(options) {
			var dfd = new $.Deferred();

			//Default value for options
			if (options === undefined) {
				options = {enableHighAccuracy: true};
			}

			navigator.geolocation.getCurrentPosition(
				function(position) { 
					dfd.resolve(position);
				}, 
				function(error) {
					dfd.reject(error);
				}, 
				options);

			return dfd.promise();
		},
        
        start: function() {
            _private.getLocation()
                .done(function(position) {
                   _private.initMap(position); 
            });
        }
    };
    
    $.extend(window, {
           onShowMap : _private.start
    });
} (jQuery, document));