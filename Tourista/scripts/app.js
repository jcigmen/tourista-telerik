(function () {


    // store a reference to the application object that will be created
    // later on so that we can use it if need be
    var app;
    var dataHotspots;
    var placeNameArray = [];
    var sampleArr = [{
        id: 1,
        name: 'Palawan'
                    }, {
        id: 2,
        name: 'Boracay'
                    }, {
        id: 3,
        name: 'Hunded Island'
                    }, {
        id: 4,
        name: 'Rice Teracces'
                    }, {
        id: 5,
        name: 'Baguio City'
                    }];
    // create an object to store the models for each view

    window.APP = {
        models: {
            hotspots: {
                title: 'Hotspots',
                ds: new kendo.data.DataSource({
                    data: placeNameArray
                }),
                alert: function (e) {
                    alert(e.data.name);
                },

            },
            landmarks: {
                title: 'Landmarks'
            },
            bucketlist: {
                title: 'Bucketlist'
            },
            about: {
                title: 'About'
            },
            home: {
                title: 'Home'
            },
            settings: {
                title: 'Settings'
            },
            contacts: {
                title: 'Contacts',
                ds: new kendo.data.DataSource({
                    data: [{
                        id: 1,
                        name: 'Bob'
                    }, {
                        id: 2,
                        name: 'Mary'
                    }, {
                        id: 3,
                        name: 'John'
                    }]
                }),
                alert: function (e) {
                    alert(e.data.name);
                }
            }
        }
    };
    /*Initialize Backend Service*/
    var apiKey = "y2a3IafHxegyE4yr";
    var el = new Everlive(apiKey);
    var hotspotsDataSource = new kendo.data.DataSource({
        type: "everlive",
        sort: {
            field: "placeName",
            dir: "asc"
        },
        transport: {
            typeName: "Place"
        }
    });

    //Adding of Data
    window.addView = kendo.observable({
        add: function () {
            if (!this.hotspot) {
                navigator.notification.alert("Please provide a hotspot name.");
                return;
            }

            hotspotsDataSource.add({
                placeName: this.hotspot,
                placeType: 'Hotspots'
            });
            hotspotsDataSource.one("sync", this.close);
            hotspotsDataSource.sync();
        },
        close: function () {
            $("#add").data("kendoMobileModalView").close();
            this.hotspot = "";
        }
    });

    // this function is called by Cordova when the application is loaded by the device
    document.addEventListener('deviceready', function () {

        window.HotspotsList = {
            data: new kendo.data.DataSource({
                type: "everlive",
                sort: {
                    field: "placeName",
                    dir: "asc"
                },
                transport: {
                    typeName: "Place"
                }
            })
        };

        app = new kendo.mobile.Application(document.body, {

            // comment out the following line to get a UI which matches the look
            // and feel of the operating system
            skin: 'flat',
            /*transition: 'slide',*/

            // the application needs to know which view to load first
            initial: 'views/landing.html'

        });

        // hide the splash screen as soon as the app is ready. otherwise
        // Cordova will wait 5 very long seconds to do it for you.
        navigator.splashscreen.hide();

        /* setTimeout(function () {
             //Get Places List
             $("#places-list").kendoMobileListView({
                 dataSource: hotspotsDataSource,
                 template: "<div class='hotspot-item'><img src='assets/hotspots/hotspots-list-image-sample.png' style='width:100%;'><br><div class='details'><strong><a href='#hotspotDetailView' data-role='button'>#: placeName #</a></strong><br>43,412 has been here</div></div>"
             });
         }, 3000);*/

    }, false);




}());